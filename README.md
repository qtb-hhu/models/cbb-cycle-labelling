# Calvin-Benson-Bassham cycle label model

Label model tracing all the carbon isotopomers of the Calvin-Benson-Bassham cycle using the [modelbase][3] package. 
Based on the the [Poolman 2000][1] implementation of the [Pettersson 1988][2] model.

## Installation

```
pip install -r requirements.txt
```

---
[1]:https://doi.org/10.1093/jexbot/51.suppl_1.319
[2]:https://doi.org/10.1111/j.1432-1033.1988.tb14242.x
[3]:https://github.com/QTB-HHU/modelbase